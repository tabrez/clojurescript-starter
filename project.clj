(defproject clojurescript-starter "0.1.0-SNAPSHOT"
  :description "Starter application for ClojureScript web application!"
  :url "http://example.com/"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.8.40"]
                 [ring "1.4.0"]
                 [reagent "0.5.1"]]

  :plugins [[lein-cljsbuild "1.1.3"]
            [lein-ring "0.9.7"]
            [lein-figwheel "0.5.2"]]

  :hooks [leiningen.cljsbuild]
  :source-paths ["src/clj"]

  :clean-targets ^{:protect false} ["target"

                                    ;; uncomment below path if you never store your js files at this path
                                    ;; instead, store them at resources/public/vendor and resources/public/externs
                                    ;; "resources/public/js"

                                    "resources/public/js/out"]
  :clean-non-project-classes true

  :cljsbuild {:builds {:dev {:source-paths ["src/cljs"]
                             :figwheel true
                             :compiler {:output-to "resources/public/js/main.js"
                                        :optimizations :none
                                        :output-dir "resources/public/js/out"
                                        :source-map true
                                        :asset-path "js/out"
                                        :main clojurescript-starter.client
                                        :pretty-print true}}

                       ;; :prod {:source-paths ["src/cljs"]
                       ;;        :jar true
                       ;;        :compiler {:output-to "resources/public/js/main.js"
                       ;;                   :optimizations :advanced
                       ;;                   :main clojurescript-starter.client}}
                       }}
  :min-lein-version "2.0.0"
  :uberjar-name "clojurescript-starter.jar"
  :main clojurescript-starter.server
  :aot [clojurescript-starter.server]
  :ring {:handler clojurescript-starter.server/app})
