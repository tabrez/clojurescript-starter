## Getting started

```
git clone https://gitlab.com/tabrez/clojurescript-starter.git  
cd clojurescript-starter  
lein cljsbuild once  
lein ring server  
```

Browse to http://localhost:3000/  
Make sure other applications are not running on port 3000.  
(Ring might start the server on a different port in that case.)  

## Alternative compilation modes

Run the following command any time you want to  rebuild the Clojurescript files:

```
lein cljsbuild once
```

Run the following command if you want to rebuild the Clojurescript files  automatically on file changes:

```
lein cljsbuild auto
```

Run the following command if you want to auto build on changes and want a browser repl:

```
lein figwheel
```

## Clean

```
lein clean
```

## Production mode

Run the following commands to generate a production version of main.js:

```
rm -f resources/public/js/main.js  
lein cljsbuild once prod
```

## Uberjar

Run the following commands to create and run an uberjar:

```
lein uberjar  
java -jar target/clojurescript-starter.jar
```

You can also push the uberjar to heroku: https://devcenter.heroku.com/articles/deploying-executable-jar-files
