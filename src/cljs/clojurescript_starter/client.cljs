(ns clojurescript-starter.client
  (:require [reagent.core :as ra :refer [atom]]))

(enable-console-print!)

(defn init []
  (print "Hello")
  (ra/render
   [:h1 "Hello world!"]
   (.getElementById js/document "my-app-area")))

(init)
